

name := "recommend"
version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  // spark core
  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "org.apache.spark" %% "spark-mllib" % "2.1.0"
)

